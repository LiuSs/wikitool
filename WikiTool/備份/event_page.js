﻿var html; //儲存回傳資訊
var bool_save = false;
function closeDIV() {
    document.getElementById("viewDIV").remove();
    document.getElementById("ttsButton").remove();
	document.getElementById("ttsButton_pause").remove();
	document.getElementById("btt_save").remove();
}

function tts_pause(){
	chrome.tts.pause();
}

function saveInfo(){
	bool_save = true;
}


//選單觸發事件
function onClickHandler(info, tab) {
    //讀取options
    var fontColor = localStorage["fontColor"];
    var fontSize = localStorage["fontSize"];
    var divBgcolor = localStorage["divBgcolor"];
    var fontFamily = localStorage["font_choose"];
    var ttsSwitch = localStorage["ttsSwitch"];
 
    //讀取被反白的字串
    var selectword = info.selectionText;

    //呼叫C# ASP.NET網頁
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:53137/api/values/" + selectword, false);
    req.send(null);
    html = req.responseText;
    
    //取得許可並插入Javascript(顯示)      
    chrome.tabs.executeScript({
        code: 'var view = document.createElement("DIV");' +
              'view.id = "viewDIV";' +
              'document.body.appendChild(view);'+              
              'document.getElementById("viewDIV").className = "viewDIV_CSS";'                    
    });
    
    //標籤建立完成後才將資料送進DIV
    chrome.tabs.executeScript({
        code: 'document.getElementById("viewDIV").innerHTML = ' + html +';'
    });
        
	
	

    if (ttsSwitch == "true") {
        chrome.tts.speak(html);
    }


    //取得許可並插入Javascript(語音)
    chrome.tabs.executeScript({
        code: 'var ts = document.createElement("BUTTON");' +
              'ts.id = "ttsButton";' +
              'document.body.appendChild(ts);' +
              'document.getElementById("ttsButton").innerHTML = "關閉";' +
              'document.getElementById("ttsButton").className = "ttsButton_CSS";'+
              'document.getElementById("ttsButton").onclick=' + closeDIV + ';'
    });
	
	 //取得許可並插入Javascript(語音暫停)
    chrome.tabs.executeScript({
        code: 'var ts_pause = document.createElement("BUTTON");' +
              'ts_pause.id = "ttsButton_pause";' +
              'document.body.appendChild(ts_pause);' +
              'document.getElementById("ttsButton_pause").innerHTML = "暫停";' +
              'document.getElementById("ttsButton_pause").className = "ttsButton_CSS";'+
              'document.getElementById("ttsButton_pause").onclick=' + tts_pause + ';'
    });	
	
	//取得許可並插入Javascript(資料儲存)
    chrome.tabs.executeScript({
        code: 'var bt_save = document.createElement("BUTTON");' +
              'bt_save.id = "btt_save";' +
              'document.body.appendChild(bt_save);' +
              'document.getElementById("btt_save").innerHTML = "儲存";' +
              'document.getElementById("btt_save").className = "ttsButton_CSS";'+
              'document.getElementById("btt_save").onclick=' + saveInfo + ';'
    });	
	
	if( bool_save == true){
		localStorage["saveHTHL"] = html;
	}
	
  	
    // 特製化viewDIV(同時定位語音按鈕)        
    chrome.tabs.insertCSS(info.id, { file: 'viewDIV.css', "allFrames": true });
    chrome.tabs.executeScript(null, { file: "viewDIV.js" });

    //動態改變CSS
    chrome.tabs.executeScript({
        code: 'var view = document.getElementById("viewDIV");' +
              'view.style.color="'+fontColor+'";'+
              'view.style.fontSize="'+fontSize+'px";'+
              'view.style.backgroundColor="' + divBgcolor + '";'
    });
	    
}

//設定選單監聽器
chrome.contextMenus.onClicked.addListener(onClickHandler);

//安裝滑鼠選單
chrome.runtime.onInstalled.addListener(function (details) {
  chrome.contextMenus.create({
    type: 'normal',
    title: 'search %s by WikiPedia.',
    id: 'WikiTool',
    contexts: ['all']
  },
  function () {     
      alert("提醒您~若要關閉小字典就按下ESC");
      alert(localStorage["saveHTML"]);
  });
});


