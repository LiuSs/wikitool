function tts_pause() {
	chrome.tts.pause();
}

function tts_resume() {
	chrome.tts.resume();
}
	
function tts_stop() {
	chrome.tts.stop();
}

function save_options() {
     localStorage["fontColor"] = document.getElementById("font_color").value;
     localStorage["fontSize"] = document.getElementById("font_size").value;
     localStorage["divBgcolor"] = document.getElementById("div_bgcolor").value;
     localStorage["font_choose"] = document.getElementById("font_choose").value;
     localStorage["ttsSwitch"] = document.getElementById("tts_switch").value;
     var num = 1
     localStorage["count"] = num;
     
}
document.getElementById("save").addEventListener("click",save_options);
document.getElementById("tts_pause").addEventListener("click",tts_pause);
document.getElementById("tts_resume").addEventListener("click",tts_resume);
document.getElementById("tts_stop").addEventListener("click",tts_stop);

document.body.onload = function () {
	 document.getElementById("font_color").value=localStorage["fontColor"];
     document.getElementById("font_size").value=localStorage["fontSize"];
     document.getElementById("div_bgcolor").value=localStorage["divBgcolor"];
     document.getElementById("font_choose").value=localStorage["font_choose"];
     document.getElementById("tts_switch").value=localStorage["ttsSwitch"];
}

