﻿var html; //儲存回傳資訊
var textSize;
var num = 1;
localStorage["num"] = num;
var num_max = 1;
localStorage["num_max"] = num_max;
var textHistory = new Array();
var count = textHistory.length;


//選單觸發事件
function onClickHandler(info, tab) {
    //讀取options
    var bt_htmlID = String(num); // 設定按鈕ID為 obj1.obj2.obj3....
    num = localStorage["num"];
    num_max = localStorage["num_max"];

    var fontColor = localStorage["fontColor"];
    var fontSize = localStorage["fontSize"];
    var divBgcolor = localStorage["divBgcolor"];
    var fontFamily = localStorage["font_choose"]; 
    var ttsSwitch = localStorage["ttsSwitch"];

    if (localStorage["fontSize"] == "12") {
        textSize = 12;
    } else if (localStorage["fontSize"] == "16") {
        textSize = 16;
    } else if (localStorage["fontSize"] == "20") {
        textSize = 20;
    } else if (localStorage["fontSize"] == "24") {
        textSize = 24;
    }

    
    
 
    //讀取被反白的字串
    var selectword = info.selectionText;
	

    //呼叫C# ASP.NET網頁
    var req = new XMLHttpRequest();
    req.open("GET", "http://localhost:53137/api/values/" + selectword, false);
    req.send(null);
    html = req.responseText;

    //取得許可並插入Javascript(顯示)      
    chrome.tabs.executeScript({
        code: 'var view = document.createElement("DIV");' +
              'view.id = "viewDIV";' +
              'document.body.appendChild(view);'+
              'document.getElementById("viewDIV").className = "viewDIV_CSS";'
              
    });

    
    chrome.tabs.executeScript({
        code: 'var bt_html = document.createElement("DIV");' +
              'bt_html.id = "' + bt_htmlID + '";' +
              'var maxCount = "'+localStorage["num_max"]+'";'+
              'if("'+localStorage["num"]+'" == 1){document.getElementById("viewDIV").appendChild(bt_html);}' +
              'else{document.getElementById("viewDIV").insertBefore(bt_html,document.getElementById(String(maxCount-1)));}' +
              'document.getElementById("' + bt_htmlID + '").innerHTML =' + html + ';' +
              'document.getElementById("' + bt_htmlID + '").width = document.getElementById("viewDIV").width;'+
              'document.getElementById("' + bt_htmlID + '").onclick = function(){document.getElementById("' + bt_htmlID + '").remove();'+
              'alert("ID:"+"'+bt_htmlID+'"+"編號:"+("' + num + '")+"最大資料數"+"'+num_max+'"+"LS_num:"+maxCount); };' +   
              'bt_html.style.fontFamily="' + fontFamily + '";'+
              'bt_html.style.color="' + fontColor + '";' +
              'bt_html.style.fontSize="' + fontSize + 'px";' +
              'bt_html.style.backgroundColor="' + divBgcolor + '";' +
              'bt_html.style.lineHeight="' + (textSize + 5) + '" + "pt";'+
              'bt_html.style.textAlign="left";'+
              'bt_html.style.borderBottomStyle = "dotted";'
    });
    localStorage["htmlWord"] = html;

    //取得許可並插入Javascript(語音)
    chrome.tabs.executeScript({
        code: 'var ts = document.createElement("BUTTON");' +
              'ts.id = "ttsButton";' +
              'document.body.appendChild(ts);' +
              'document.getElementById("ttsButton").innerHTML = "關閉";' +
              'document.getElementById("ttsButton").className = "ttsButton_CSS";'+
              'document.getElementById("ttsButton").onclick=function(){document.getElementById("viewDIV").remove();'+
  	  		  'document.getElementById("ttsButton").remove(); document.getElementById("bt_end").remove();};'
    });

	if (ttsSwitch == "true") {
	    chrome.tts.speak(html);
    }
	

	
  	
    // 特製化viewDIV(同時定位語音按鈕)        
    chrome.tabs.insertCSS(info.id, { file: 'viewDIV.css', "allFrames": true });
    chrome.tabs.executeScript(null, { file: "viewDIV.js" });

    //動態改變CSS
    chrome.tabs.executeScript({
        code: 'var view = document.getElementById("viewDIV");' +
              'view.style.color="'+fontColor+'";'+
              'view.style.fontSize="'+fontSize+'px";'+
              'view.style.backgroundColor="' + divBgcolor + '";'+
              'view.style.lineHeight="'+(textSize+5)+'" + "pt";'+
              'view.style.fontFamily="' + fontFamily + '";'
    }); 
 
    alert(bt_htmlID + localStorage["num_max"]);

    if (num <= 10) {
        num_max++;
        num++;
    } else {
        alert("最大查詢數目為10筆");
        num_max=0;
        num=0;
    }

     localStorage["num_max"] = num_max;
     localStorage["num"] = num;
    
}




//設定選單監聽器
chrome.contextMenus.onClicked.addListener(onClickHandler);

//安裝滑鼠選單
chrome.runtime.onInstalled.addListener(function (details) {
  chrome.contextMenus.create({
    type: 'normal',
    title: 'search %s by WikiPedia.',
    id: 'WikiTool',
    contexts: ['all']
  },
  function () {     
      alert("提醒您~若要關閉小字典就按下ESC");
  });
});


