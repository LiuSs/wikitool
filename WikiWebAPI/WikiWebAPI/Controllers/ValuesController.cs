﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//額外新增的工具
using System.Net;//HTTP協定
using System.IO;//串流轉換工具
using System.Text;//編碼工具
using HtmlAgilityPack;//非同步擷取網頁文件
using System.Xml.XPath;//XPath格式
using System.Xml;


namespace WikiWebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/string
        public string Get(string id)
        {   
            string getString = id;
            Encoding encode_method = Encoding.GetEncoding("utf-8");//宣告欲使用的編碼方式            
            string input = HttpUtility.UrlEncode(getString, encode_method);
            string wikiText="";

            HtmlWeb client = new HtmlWeb();
            HtmlDocument doc = client.Load("https://zh.wikipedia.org/zh-tw/" + input);//抓取網頁文件

            try
            {
                for (int i = 1; i < 2; i++)
                {
                    var XpathString = "//div[@class='mw-content-ltr']/p[1]";
                    HtmlNodeCollection nameNodes = doc.DocumentNode.SelectNodes(XpathString);//使用HAP工具搭配XPath格式

                    foreach (HtmlNode wikiCont in nameNodes)//將抓到節點內的文字依序輸出
                    {                        
                        wikiText = wikiText + wikiCont.InnerText;
                    }
                }
                if (wikiText == "\"\"")
                {
                    return "抱歉，尚無相關資訊可供參考\n";
                }
                return wikiText;
            }
            catch(Exception e)
            {
                return wikiText;
            }
          
        }
    }
}